import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title: string = 'client';
  sideBarActive: boolean = false;
  @Output() toggleMenu = new EventEmitter();

  sidebarToggle() {
    this.sideBarActive = !this.sideBarActive;
    this.toggleMenu.emit(this.sideBarActive);
  }
}
